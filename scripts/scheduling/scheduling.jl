### A Pluto.jl notebook ###
# v0.19.38

using Markdown
using InteractiveUtils

# ╔═╡ fab9e974-6350-4a29-866b-e41eb2d32866
using CSV

# ╔═╡ 5c14f53f-c8bc-4d41-80d9-5a4861bbec75
using DataFrames

# ╔═╡ 0bfe1a88-0ee4-41b6-a343-480cefcd5f69
using LinearAlgebra

# ╔═╡ a2b9fb91-19f7-43aa-82de-891747fd3271
using JuMP

# ╔═╡ 34784318-558e-4b5c-a3eb-63edfea858f9
using Mosek

# ╔═╡ 4560730f-8e4e-4268-ae56-9e8f7220d580
using MosekTools

# ╔═╡ 05b38db8-c20d-11ee-26b0-411e18e34b64
md"""

## Step 1

!!! info "Downloading data from when2meet"
    It is easier to retrieve the data automatically than transcribing them manually. 

You can download the data by executing the following [script](https://gist.github.com/camtheman256/3125e18ba20e90b6252678714e5102fd) on the website. 
```javascript
function getCSV() {
  result = "Time," + PeopleNames.join(",")+"\n"; 
  for(let i = 0; i < AvailableAtSlot.length; i++) {
      let slot = $x(`string(//div[@id="GroupTime${TimeOfSlot[i]}"]/@onmouseover)`);
      slot = slot.match(/.*"(.*)".*/)[1];
      result += slot + ",";
      result += PeopleIDs.map(id => AvailableAtSlot[i].includes(id) ? 1 : 0).join(",");
      result+= "\n";
  }
  console.log(result);
}
getCSV();
```
"""

# ╔═╡ 0c03e004-49b7-4021-8e3f-335d4c66af5c
md"""
## Step 2

We will now load the string output from the CSV into Julia.
"""

# ╔═╡ 220ec4af-eba6-4bd4-99f0-24d711f98fbf
availability_str = 
"""
Time,Caleb Hill,Charles,Gabby Trudeau,Gersi,Jason Londono,Kripesh D,kyle ball,Maeve Burwell,Michaela power,Muthanna Kareem,Patrick Doherty,ryan b,sam
Monday 09:00:00 AM,1,0,1,1,0,0,0,0,0,0,0,0,1
Monday 09:15:00 AM,1,0,1,1,0,0,0,0,0,0,0,0,1
Monday 09:30:00 AM,1,0,1,1,0,1,0,0,0,0,0,0,1
Monday 09:45:00 AM,1,0,1,1,0,1,0,0,0,0,0,0,1
Monday 10:00:00 AM,0,0,0,0,0,0,0,0,0,0,0,0,0
Monday 10:15:00 AM,0,0,0,0,0,0,0,0,0,0,0,0,0
Monday 10:30:00 AM,0,0,0,0,0,0,0,0,0,0,0,0,0
Monday 10:45:00 AM,0,0,0,0,0,0,0,0,0,0,0,0,0
Monday 11:00:00 AM,0,0,1,1,1,0,0,0,1,0,0,1,0
Monday 11:15:00 AM,0,0,1,1,1,0,0,0,1,0,0,1,0
Monday 11:30:00 AM,0,0,1,1,1,0,0,0,1,0,0,1,0
Monday 11:45:00 AM,0,0,1,1,1,0,0,0,1,0,0,1,0
Monday 12:00:00 PM,0,0,0,1,1,0,0,0,0,0,0,1,0
Monday 12:15:00 PM,0,0,0,1,1,0,0,0,0,0,0,1,0
Monday 12:30:00 PM,1,0,0,1,0,0,0,0,0,0,0,1,0
Monday 12:45:00 PM,1,0,0,1,0,0,0,0,0,0,0,1,0
Monday 01:00:00 PM,1,1,0,0,0,0,0,1,0,0,1,1,0
Monday 01:15:00 PM,1,1,0,0,0,0,0,1,0,0,1,1,0
Monday 01:30:00 PM,1,1,0,0,0,0,0,1,0,0,1,1,0
Monday 01:45:00 PM,1,1,0,0,0,0,0,1,0,0,1,1,0
Monday 02:00:00 PM,1,1,1,0,0,0,0,0,0,0,1,1,1
Monday 02:15:00 PM,1,1,1,0,0,0,0,0,0,0,1,1,1
Monday 02:30:00 PM,1,1,1,0,0,0,0,0,0,0,1,1,1
Monday 02:45:00 PM,1,1,1,0,0,0,0,0,0,0,1,1,1
Monday 03:00:00 PM,1,1,1,0,0,0,0,0,0,0,1,1,1
Monday 03:15:00 PM,1,1,1,0,0,0,0,0,0,0,1,1,1
Monday 03:30:00 PM,1,1,1,0,0,0,0,1,0,0,0,1,1
Monday 03:45:00 PM,1,1,1,0,0,0,0,1,0,0,0,1,1
Monday 04:00:00 PM,1,1,1,0,0,1,1,0,0,0,0,1,1
Monday 04:15:00 PM,1,1,1,0,0,1,1,0,0,0,0,1,1
Monday 04:30:00 PM,1,1,1,0,0,1,1,0,0,0,0,1,1
Monday 04:45:00 PM,1,1,1,0,0,1,1,0,0,0,0,1,1
Tuesday 09:00:00 AM,1,1,1,0,1,0,0,0,0,0,1,0,1
Tuesday 09:15:00 AM,1,1,1,0,1,0,0,0,1,0,1,0,1
Tuesday 09:30:00 AM,1,1,1,0,1,0,0,0,1,0,1,0,1
Tuesday 09:45:00 AM,1,1,1,0,1,0,0,0,1,0,1,0,1
Tuesday 10:00:00 AM,1,1,1,0,1,0,0,0,0,0,1,1,1
Tuesday 10:15:00 AM,1,1,1,0,1,0,0,0,0,0,1,1,1
Tuesday 10:30:00 AM,1,1,1,0,1,0,0,0,0,0,1,1,1
Tuesday 10:45:00 AM,1,1,1,0,1,0,0,0,0,0,1,1,1
Tuesday 11:00:00 AM,1,1,1,0,0,0,0,0,0,0,1,1,1
Tuesday 11:15:00 AM,1,1,1,0,0,0,0,0,0,0,1,1,1
Tuesday 11:30:00 AM,1,1,1,0,0,0,0,0,0,0,1,1,1
Tuesday 11:45:00 AM,1,1,1,0,0,0,0,0,0,0,1,1,1
Tuesday 12:00:00 PM,0,1,1,0,0,0,0,0,0,0,1,1,1
Tuesday 12:15:00 PM,0,1,1,0,0,0,0,0,0,0,1,1,1
Tuesday 12:30:00 PM,0,1,1,1,1,0,0,0,0,0,1,1,1
Tuesday 12:45:00 PM,0,1,1,1,1,0,0,0,0,0,1,1,1
Tuesday 01:00:00 PM,0,1,1,1,1,0,0,0,0,0,1,1,1
Tuesday 01:15:00 PM,0,1,1,1,1,0,0,0,0,0,1,1,1
Tuesday 01:30:00 PM,0,1,1,1,1,0,0,0,0,0,1,1,1
Tuesday 01:45:00 PM,0,1,1,1,1,0,0,0,0,0,1,1,1
Tuesday 02:00:00 PM,0,1,0,0,0,0,0,0,0,0,0,1,1
Tuesday 02:15:00 PM,0,1,0,0,0,0,0,0,0,0,0,1,1
Tuesday 02:30:00 PM,0,1,0,0,0,0,0,0,0,0,0,1,1
Tuesday 02:45:00 PM,0,1,0,0,0,0,0,0,0,0,0,1,1
Tuesday 03:00:00 PM,0,1,0,0,0,0,0,0,0,0,0,1,1
Tuesday 03:15:00 PM,0,1,0,0,0,0,0,0,0,0,0,1,1
Tuesday 03:30:00 PM,0,1,0,0,0,0,0,1,0,0,0,1,0
Tuesday 03:45:00 PM,0,1,0,0,0,0,0,1,0,0,0,1,0
Tuesday 04:00:00 PM,0,1,0,0,0,0,1,0,0,0,0,0,0
Tuesday 04:15:00 PM,0,1,0,0,0,0,1,0,0,0,0,0,0
Tuesday 04:30:00 PM,0,1,0,0,0,0,1,0,0,0,0,0,0
Tuesday 04:45:00 PM,0,1,0,0,0,0,1,0,0,0,0,0,0
Wednesday 09:00:00 AM,1,0,1,1,0,0,0,0,0,0,0,0,1
Wednesday 09:15:00 AM,1,0,1,1,0,0,0,0,0,0,0,0,1
Wednesday 09:30:00 AM,1,0,1,1,0,1,0,0,0,0,0,0,1
Wednesday 09:45:00 AM,1,0,1,1,0,1,0,0,0,0,0,0,1
Wednesday 10:00:00 AM,0,0,0,0,0,0,0,0,0,0,0,0,0
Wednesday 10:15:00 AM,0,0,0,0,0,0,0,0,0,0,0,0,0
Wednesday 10:30:00 AM,0,0,0,0,0,0,0,0,0,0,0,0,0
Wednesday 10:45:00 AM,0,0,0,0,0,0,0,0,0,0,0,0,0
Wednesday 11:00:00 AM,0,0,1,1,1,0,0,0,1,0,0,0,0
Wednesday 11:15:00 AM,0,0,1,1,1,0,0,0,1,0,0,0,0
Wednesday 11:30:00 AM,0,0,1,1,1,0,0,0,1,0,0,0,0
Wednesday 11:45:00 AM,0,0,1,1,1,0,0,0,1,0,0,0,0
Wednesday 12:00:00 PM,0,0,0,1,1,0,0,0,0,0,0,1,0
Wednesday 12:15:00 PM,0,0,0,1,1,0,0,0,0,0,0,1,0
Wednesday 12:30:00 PM,1,0,0,1,0,0,0,0,0,0,0,1,0
Wednesday 12:45:00 PM,1,0,0,1,0,0,0,0,0,0,0,1,0
Wednesday 01:00:00 PM,1,1,0,0,0,0,0,1,0,0,1,1,0
Wednesday 01:15:00 PM,1,1,0,0,0,0,0,1,0,0,1,1,0
Wednesday 01:30:00 PM,1,1,0,0,0,0,0,1,0,0,1,1,0
Wednesday 01:45:00 PM,1,1,0,0,0,0,0,1,0,0,1,1,0
Wednesday 02:00:00 PM,1,1,1,0,0,1,0,0,0,0,1,1,1
Wednesday 02:15:00 PM,1,1,1,0,0,1,0,0,0,0,1,1,1
Wednesday 02:30:00 PM,1,1,1,0,0,1,0,0,0,0,1,1,1
Wednesday 02:45:00 PM,1,1,1,0,0,1,0,0,0,0,1,1,1
Wednesday 03:00:00 PM,1,1,1,0,0,1,0,0,0,0,1,1,1
Wednesday 03:15:00 PM,1,1,1,0,0,1,0,0,0,0,1,1,1
Wednesday 03:30:00 PM,1,1,1,0,0,1,0,0,0,0,0,1,1
Wednesday 03:45:00 PM,1,1,1,0,0,1,0,0,0,0,0,1,1
Wednesday 04:00:00 PM,1,1,1,0,0,1,1,0,0,0,0,0,1
Wednesday 04:15:00 PM,1,1,1,0,0,1,1,0,0,0,0,0,1
Wednesday 04:30:00 PM,1,1,1,0,0,1,1,0,0,0,0,0,1
Wednesday 04:45:00 PM,1,1,1,0,0,1,1,0,0,0,0,0,1
Thursday 09:00:00 AM,1,1,0,0,1,0,1,0,0,0,1,0,1
Thursday 09:15:00 AM,1,1,0,0,1,0,1,0,1,0,1,0,1
Thursday 09:30:00 AM,1,1,0,0,1,0,1,0,1,0,1,0,1
Thursday 09:45:00 AM,1,1,0,0,1,0,1,0,1,0,1,0,1
Thursday 10:00:00 AM,1,1,0,0,1,0,0,0,0,0,1,1,1
Thursday 10:15:00 AM,0,1,0,0,1,0,0,0,0,0,1,1,1
Thursday 10:30:00 AM,0,1,0,0,1,0,0,0,0,0,1,1,1
Thursday 10:45:00 AM,0,1,0,0,1,0,0,0,0,0,1,1,1
Thursday 11:00:00 AM,0,1,0,0,0,0,0,0,0,0,1,1,0
Thursday 11:15:00 AM,0,1,0,0,0,0,0,0,0,0,1,1,0
Thursday 11:30:00 AM,0,1,0,0,0,0,0,0,0,0,1,1,1
Thursday 11:45:00 AM,0,1,0,0,0,0,0,0,0,0,1,1,1
Thursday 12:00:00 PM,1,1,0,0,0,0,0,1,0,0,1,1,1
Thursday 12:15:00 PM,1,1,0,0,0,0,0,1,0,0,1,1,1
Thursday 12:30:00 PM,1,1,0,0,0,0,0,1,0,0,1,1,1
Thursday 12:45:00 PM,1,1,0,0,0,0,0,1,0,0,1,1,1
Thursday 01:00:00 PM,1,1,0,0,0,0,0,0,0,0,1,1,1
Thursday 01:15:00 PM,1,1,0,0,0,0,0,0,0,0,1,1,1
Thursday 01:30:00 PM,1,1,0,0,0,0,0,0,0,0,1,1,1
Thursday 01:45:00 PM,1,1,0,0,0,0,0,0,0,0,1,1,1
Thursday 02:00:00 PM,1,1,0,0,0,0,0,0,0,0,0,1,1
Thursday 02:15:00 PM,1,1,0,0,0,0,0,0,0,0,0,1,1
Thursday 02:30:00 PM,1,1,0,0,0,0,0,0,0,0,0,1,1
Thursday 02:45:00 PM,1,1,0,0,0,0,0,0,0,0,0,1,1
Thursday 03:00:00 PM,1,1,0,0,0,0,1,1,0,0,0,1,1
Thursday 03:15:00 PM,1,1,0,0,0,0,1,1,0,0,0,1,1
Thursday 03:30:00 PM,1,1,0,0,0,0,1,0,0,0,0,1,1
Thursday 03:45:00 PM,1,1,0,0,0,0,1,0,0,0,0,1,1
Thursday 04:00:00 PM,1,1,0,0,0,0,1,0,0,0,0,0,1
Thursday 04:15:00 PM,1,1,0,0,0,0,1,0,0,0,0,0,1
Thursday 04:30:00 PM,1,1,0,0,0,0,1,0,0,0,0,0,1
Thursday 04:45:00 PM,1,1,0,0,0,0,1,0,0,0,0,0,1
Friday 09:00:00 AM,1,0,1,1,0,0,0,0,0,0,0,0,1
Friday 09:15:00 AM,1,0,1,1,0,0,0,0,0,0,0,0,1
Friday 09:30:00 AM,1,0,1,1,0,1,0,0,0,0,0,0,1
Friday 09:45:00 AM,1,0,1,1,0,1,0,0,0,0,0,0,1
Friday 10:00:00 AM,0,0,0,0,0,0,0,0,0,0,0,0,0
Friday 10:15:00 AM,0,0,0,0,0,0,0,0,0,0,0,0,0
Friday 10:30:00 AM,0,0,0,0,0,0,0,0,0,0,0,0,0
Friday 10:45:00 AM,0,0,0,0,0,0,0,0,0,0,0,0,0
Friday 11:00:00 AM,0,0,1,0,1,0,0,0,0,0,0,1,0
Friday 11:15:00 AM,0,0,1,0,1,0,0,0,0,0,0,1,0
Friday 11:30:00 AM,0,0,1,0,1,0,0,0,0,0,0,1,0
Friday 11:45:00 AM,0,0,1,0,1,0,0,0,0,0,0,1,0
Friday 12:00:00 PM,0,0,0,1,1,0,0,0,0,0,0,1,0
Friday 12:15:00 PM,0,0,0,1,1,0,0,0,0,0,0,1,0
Friday 12:30:00 PM,1,0,0,1,1,1,0,1,0,0,0,1,0
Friday 12:45:00 PM,1,0,0,1,1,1,0,1,0,0,0,1,0
Friday 01:00:00 PM,1,0,0,1,1,1,0,0,0,0,1,1,1
Friday 01:15:00 PM,1,0,0,1,1,1,0,0,0,0,1,1,1
Friday 01:30:00 PM,1,0,0,1,1,1,1,0,0,0,1,1,1
Friday 01:45:00 PM,1,0,0,1,1,1,0,0,0,0,1,0,1
Friday 02:00:00 PM,1,0,0,0,0,1,1,0,0,0,1,0,1
Friday 02:15:00 PM,1,0,0,0,0,1,1,0,0,0,1,0,1
Friday 02:30:00 PM,1,0,0,0,0,1,1,1,0,0,1,0,1
Friday 02:45:00 PM,1,0,0,0,0,1,1,1,0,0,1,0,1
Friday 03:00:00 PM,1,1,0,0,0,1,1,1,0,0,1,0,1
Friday 03:15:00 PM,1,1,0,0,0,1,1,1,0,0,1,0,1
Friday 03:30:00 PM,1,1,0,0,1,1,1,1,0,0,0,0,1
Friday 03:45:00 PM,1,1,0,0,1,1,1,1,0,0,0,0,1
Friday 04:00:00 PM,1,1,0,0,1,1,1,0,0,0,0,0,1
Friday 04:15:00 PM,1,1,0,0,1,1,1,0,0,0,0,0,1
Friday 04:30:00 PM,1,1,0,0,1,1,1,0,0,0,0,0,1
Friday 04:45:00 PM,1,1,0,0,1,1,1,0,0,0,0,0,1
"""

# ╔═╡ e27da409-8d36-46f2-9bdd-64653f113009
availability = CSV.read(IOBuffer(availability_str), DataFrame)

# ╔═╡ 647e16a3-7c4a-4440-8e37-fb59b4cc8832
zip(names(availability)[2:end], map(sum, eachcol(availability[:,2:end]))) |> collect

# ╔═╡ 5cfad693-3690-4624-aafe-b24dfc45958a
md"""
## Step 3

Formulate and optimization problem. I define one variable for each time period representing a possible time to hold an office hour. 

We need to load some packages.
"""

# ╔═╡ d384bede-389c-4e15-bde7-3f17a73a7e88
md"""
The optimization model can be constructed as follows. The **variables** are:

- Binary variable ``x`` indicates for each time period whether office hours are held during that 15 minute time period
- Binary variable ``s`` indicates for each time period whether office hours start at that time period. This variable is needed because each office hour needs to span four 15 minute periods.
- Variable ``z`` indicates each person's deficit from having two office hours

The **constraints** are as follows:

- Office hour lasts for one hour 
- Each person needs to be able to attend at least one office hour
- The instructor needs to be available for that hour

The **objective** is to minimize the squares of the deficits ``z``.
"""

# ╔═╡ 964788c3-bd1f-4ce0-a462-52fce10099e7
md"""
The rows in the **availability matrix** ``A`` represent times and columns represent people.
"""

# ╔═╡ 4cc7ea24-9ea4-4f6e-a5ec-4be45f08186a
A = Matrix(availability[:,2:end])

# ╔═╡ 5f0bff46-e908-460b-a95f-e038e705b30f
begin
	model = Model(Mosek.Optimizer)
	@variable(model, x[i=1:size(A,1)], Bin)
	@variable(model, s[i=1:size(A,1)], Bin)	
	@variable(model, 0 ≤ z[i=1:size(A,2)])

	n = size(A, 1)
	@constraint(model, z .≥ 4 .- A'*x ) # everyone can attend an office hour
	@constraint(model, sum(s) ≤ 2) # at most two office hours
	@constraint(model, [j=1:n], x[j] ≤ sum(s[max(j-3,1):j])) # office hour = 1h

	@objective(model, Min, z⋅z) # maximize contact hours
end

# ╔═╡ 811d17a7-fd3a-457a-b7e8-4f6e6d22d101
optimize!(model)

# ╔═╡ 44821523-66ed-4f1a-8374-9798097b041f
"The solution status is: $(termination_status(model))"

# ╔═╡ e17f7f32-9e8c-4d3c-9810-36a3f54b5464
md"""
## Solution
"""

# ╔═╡ 5b45dd03-7218-456a-ab6c-5fd65ec1bb51
md"""
Students' deficits from 2 hours of availability are as follows.
"""

# ╔═╡ aaab6f18-8297-490b-a122-ca6175c77aa6
zip(names(availability)[2:end],round.(value.(z)) / 4) |> collect

# ╔═╡ d32f1afc-9c93-423e-b550-da6bd80e0e28
md"""
My office hours should be held at the following times to maximize my availability.
"""

# ╔═╡ a0e4962c-c549-4a15-9fec-927c33df8dc1
DataFrame(Time=availability.Time,  Office=value.(x)) |> 
         f->subset(f, :Office => x -> x.> 0)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
CSV = "336ed68f-0bac-5ca0-87d4-7b16caf5d00b"
DataFrames = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
JuMP = "4076af6c-e467-56ae-b986-b466b2749572"
LinearAlgebra = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"
Mosek = "6405355b-0ac2-5fba-af84-adbd65488c0e"
MosekTools = "1ec41992-ff65-5c91-ac43-2df89e9693a4"

[compat]
CSV = "~0.10.12"
DataFrames = "~1.6.1"
JuMP = "~1.19.0"
Mosek = "~10.1.3"
MosekTools = "~0.15.1"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.10.0"
manifest_format = "2.0"
project_hash = "a8bf8cf8b45aabf25902515213760620d38c913c"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"
version = "1.1.1"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "f1f03a9fa24271160ed7e73051fba3c1a759b53f"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.4.0"

[[deps.Bzip2_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "9e2a6b69137e6969bab0152632dcb3bc108c8bdd"
uuid = "6e34b625-4abd-537c-b88f-471c36dfa7a0"
version = "1.0.8+1"

[[deps.CSV]]
deps = ["CodecZlib", "Dates", "FilePathsBase", "InlineStrings", "Mmap", "Parsers", "PooledArrays", "PrecompileTools", "SentinelArrays", "Tables", "Unicode", "WeakRefStrings", "WorkerUtilities"]
git-tree-sha1 = "679e69c611fff422038e9e21e270c4197d49d918"
uuid = "336ed68f-0bac-5ca0-87d4-7b16caf5d00b"
version = "0.10.12"

[[deps.CodecBzip2]]
deps = ["Bzip2_jll", "Libdl", "TranscodingStreams"]
git-tree-sha1 = "9b1ca1aa6ce3f71b3d1840c538a8210a043625eb"
uuid = "523fee87-0ab8-5b00-afb7-3ecf72e48cfd"
version = "0.8.2"

[[deps.CodecZlib]]
deps = ["TranscodingStreams", "Zlib_jll"]
git-tree-sha1 = "59939d8a997469ee05c4b4944560a820f9ba0d73"
uuid = "944b1d66-785c-5afd-91f1-9de20f533193"
version = "0.7.4"

[[deps.CommonSubexpressions]]
deps = ["MacroTools", "Test"]
git-tree-sha1 = "7b8a93dba8af7e3b42fecabf646260105ac373f7"
uuid = "bbf7d656-a473-5ed7-a52c-81e309532950"
version = "0.3.0"

[[deps.Compat]]
deps = ["TOML", "UUIDs"]
git-tree-sha1 = "75bd5b6fc5089df449b5d35fa501c846c9b6549b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "4.12.0"
weakdeps = ["Dates", "LinearAlgebra"]

    [deps.Compat.extensions]
    CompatLinearAlgebraExt = "LinearAlgebra"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "1.0.5+1"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.DataAPI]]
git-tree-sha1 = "abe83f3a2f1b857aac70ef8b269080af17764bbe"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.16.0"

[[deps.DataFrames]]
deps = ["Compat", "DataAPI", "DataStructures", "Future", "InlineStrings", "InvertedIndices", "IteratorInterfaceExtensions", "LinearAlgebra", "Markdown", "Missings", "PooledArrays", "PrecompileTools", "PrettyTables", "Printf", "REPL", "Random", "Reexport", "SentinelArrays", "SortingAlgorithms", "Statistics", "TableTraits", "Tables", "Unicode"]
git-tree-sha1 = "04c738083f29f86e62c8afc341f0967d8717bdb8"
uuid = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
version = "1.6.1"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "ac67408d9ddf207de5cfa9a97e114352430f01ed"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.16"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DiffResults]]
deps = ["StaticArraysCore"]
git-tree-sha1 = "782dd5f4561f5d267313f23853baaaa4c52ea621"
uuid = "163ba53b-c6d8-5494-b064-1a9d43ac40c5"
version = "1.1.0"

[[deps.DiffRules]]
deps = ["IrrationalConstants", "LogExpFunctions", "NaNMath", "Random", "SpecialFunctions"]
git-tree-sha1 = "23163d55f885173722d1e4cf0f6110cdbaf7e272"
uuid = "b552c78f-8df3-52c6-915a-8e097449b14b"
version = "1.15.1"

[[deps.DocStringExtensions]]
deps = ["LibGit2"]
git-tree-sha1 = "2fb1e02f2b635d0845df5d7c167fec4dd739b00d"
uuid = "ffbed154-4ef7-542d-bbb7-c09d3a79fcae"
version = "0.9.3"

[[deps.Downloads]]
deps = ["ArgTools", "FileWatching", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"
version = "1.6.0"

[[deps.FilePathsBase]]
deps = ["Compat", "Dates", "Mmap", "Printf", "Test", "UUIDs"]
git-tree-sha1 = "9f00e42f8d99fdde64d40c8ea5d14269a2e2c1aa"
uuid = "48062228-2e41-5def-b9a4-89aafe57970f"
version = "0.9.21"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.ForwardDiff]]
deps = ["CommonSubexpressions", "DiffResults", "DiffRules", "LinearAlgebra", "LogExpFunctions", "NaNMath", "Preferences", "Printf", "Random", "SpecialFunctions"]
git-tree-sha1 = "cf0fe81336da9fb90944683b8c41984b08793dad"
uuid = "f6369f11-7733-5829-9624-2563aa707210"
version = "0.10.36"

    [deps.ForwardDiff.extensions]
    ForwardDiffStaticArraysExt = "StaticArrays"

    [deps.ForwardDiff.weakdeps]
    StaticArrays = "90137ffa-7385-5640-81b9-e52037218182"

[[deps.Future]]
deps = ["Random"]
uuid = "9fa8497b-333b-5362-9e8d-4d0656e87820"

[[deps.InlineStrings]]
deps = ["Parsers"]
git-tree-sha1 = "9cc2baf75c6d09f9da536ddf58eb2f29dedaf461"
uuid = "842dd82b-1e85-43dc-bf29-5d0ee9dffc48"
version = "1.4.0"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.InvertedIndices]]
git-tree-sha1 = "0dc7b50b8d436461be01300fd8cd45aa0274b038"
uuid = "41ab1584-1d38-5bbf-9106-f11c6c58b48f"
version = "1.3.0"

[[deps.IrrationalConstants]]
git-tree-sha1 = "630b497eafcc20001bba38a4651b327dcfc491d2"
uuid = "92d709cd-6900-40b7-9082-c6be49f344b6"
version = "0.2.2"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.JLLWrappers]]
deps = ["Artifacts", "Preferences"]
git-tree-sha1 = "7e5d6779a1e09a36db2a7b6cff50942a0a7d0fca"
uuid = "692b3bcd-3c85-4b1f-b108-f13ce0eb3210"
version = "1.5.0"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "31e996f0a15c7b280ba9f76636b3ff9e2ae58c9a"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.4"

[[deps.JuMP]]
deps = ["LinearAlgebra", "MacroTools", "MathOptInterface", "MutableArithmetics", "OrderedCollections", "PrecompileTools", "Printf", "SparseArrays"]
git-tree-sha1 = "5036b4cf6d85b08d80de09ef65b4d951f6e68659"
uuid = "4076af6c-e467-56ae-b986-b466b2749572"
version = "1.19.0"

    [deps.JuMP.extensions]
    JuMPDimensionalDataExt = "DimensionalData"

    [deps.JuMP.weakdeps]
    DimensionalData = "0703355e-b756-11e9-17c0-8b28908087d0"

[[deps.LaTeXStrings]]
git-tree-sha1 = "50901ebc375ed41dbf8058da26f9de442febbbec"
uuid = "b964fa9f-0449-5b57-a5c2-d3ea65f4040f"
version = "1.3.1"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"
version = "0.6.4"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"
version = "8.4.0+0"

[[deps.LibGit2]]
deps = ["Base64", "LibGit2_jll", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibGit2_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll"]
uuid = "e37daf67-58a4-590a-8e99-b0245dd2ffc5"
version = "1.6.4+0"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"
version = "1.11.0+1"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "OpenBLAS_jll", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.LogExpFunctions]]
deps = ["DocStringExtensions", "IrrationalConstants", "LinearAlgebra"]
git-tree-sha1 = "7d6dd4e9212aebaeed356de34ccf262a3cd415aa"
uuid = "2ab3a3ac-af41-5b50-aa03-7779005ae688"
version = "0.3.26"

    [deps.LogExpFunctions.extensions]
    LogExpFunctionsChainRulesCoreExt = "ChainRulesCore"
    LogExpFunctionsChangesOfVariablesExt = "ChangesOfVariables"
    LogExpFunctionsInverseFunctionsExt = "InverseFunctions"

    [deps.LogExpFunctions.weakdeps]
    ChainRulesCore = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
    ChangesOfVariables = "9e997f8a-9a97-42d5-a9f1-ce6bfc15e2c0"
    InverseFunctions = "3587e190-3f89-42d0-90ee-14403ec27112"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "2fa9ee3e63fd3a4f7a9a4f4744a52f4856de82df"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.13"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MathOptInterface]]
deps = ["BenchmarkTools", "CodecBzip2", "CodecZlib", "DataStructures", "ForwardDiff", "JSON", "LinearAlgebra", "MutableArithmetics", "NaNMath", "OrderedCollections", "PrecompileTools", "Printf", "SparseArrays", "SpecialFunctions", "Test", "Unicode"]
git-tree-sha1 = "8b40681684df46785a0012d352982e22ac3be59e"
uuid = "b8f27783-ece8-5eb3-8dc8-9495eed66fee"
version = "1.25.2"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"
version = "2.28.2+1"

[[deps.Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "f66bdc5de519e8f8ae43bdc598782d35a25b1272"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.1.0"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.Mosek]]
deps = ["Libdl", "Pkg", "Printf", "SparseArrays"]
git-tree-sha1 = "a85deb4957c1f2ad5cd7785e5db8d61a722d8604"
uuid = "6405355b-0ac2-5fba-af84-adbd65488c0e"
version = "10.1.3"

[[deps.MosekTools]]
deps = ["MathOptInterface", "Mosek", "Printf"]
git-tree-sha1 = "db3472bbf2d7565c895470e97ccd3834ae663282"
uuid = "1ec41992-ff65-5c91-ac43-2df89e9693a4"
version = "0.15.1"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"
version = "2023.1.10"

[[deps.MutableArithmetics]]
deps = ["LinearAlgebra", "SparseArrays", "Test"]
git-tree-sha1 = "806eea990fb41f9b36f1253e5697aa645bf6a9f8"
uuid = "d8a4904e-b15c-11e9-3269-09a3773c0cb0"
version = "1.4.0"

[[deps.NaNMath]]
deps = ["OpenLibm_jll"]
git-tree-sha1 = "0877504529a3e5c3343c6f8b4c0381e57e4387e4"
uuid = "77ba4419-2d1f-58cd-9bb1-8ffee604a2e3"
version = "1.0.2"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"
version = "1.2.0"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.23+2"

[[deps.OpenLibm_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "05823500-19ac-5b8b-9628-191a04bc5112"
version = "0.8.1+2"

[[deps.OpenSpecFun_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "13652491f6856acfd2db29360e1bbcd4565d04f1"
uuid = "efe28fd5-8261-553b-a9e1-b2916fc3738e"
version = "0.5.5+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "dfdf5519f235516220579f949664f1bf44e741c5"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.6.3"

[[deps.Parsers]]
deps = ["Dates", "PrecompileTools", "UUIDs"]
git-tree-sha1 = "8489905bcdbcfac64d1daa51ca07c0d8f0283821"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.8.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "FileWatching", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
version = "1.10.0"

[[deps.PooledArrays]]
deps = ["DataAPI", "Future"]
git-tree-sha1 = "36d8b4b899628fb92c2749eb488d884a926614d3"
uuid = "2dfb63ee-cc39-5dd5-95bd-886bf059d720"
version = "1.4.3"

[[deps.PrecompileTools]]
deps = ["Preferences"]
git-tree-sha1 = "03b4c25b43cb84cee5c90aa9b5ea0a78fd848d2f"
uuid = "aea7be01-6a6a-4083-8856-8a6e6704d82a"
version = "1.2.0"

[[deps.Preferences]]
deps = ["TOML"]
git-tree-sha1 = "00805cd429dcb4870060ff49ef443486c262e38e"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.4.1"

[[deps.PrettyTables]]
deps = ["Crayons", "LaTeXStrings", "Markdown", "PrecompileTools", "Printf", "Reexport", "StringManipulation", "Tables"]
git-tree-sha1 = "88b895d13d53b5577fd53379d913b9ab9ac82660"
uuid = "08abe8d2-0d0c-5749-adfa-8a2ac140af0d"
version = "2.3.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.SentinelArrays]]
deps = ["Dates", "Random"]
git-tree-sha1 = "0e7508ff27ba32f26cd459474ca2ede1bc10991f"
uuid = "91c51154-3ec4-41a3-a24f-3f23e20d615c"
version = "1.4.1"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "66e0a8e672a0bdfca2c3f5937efb8538b9ddc085"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.2.1"

[[deps.SparseArrays]]
deps = ["Libdl", "LinearAlgebra", "Random", "Serialization", "SuiteSparse_jll"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"
version = "1.10.0"

[[deps.SpecialFunctions]]
deps = ["IrrationalConstants", "LogExpFunctions", "OpenLibm_jll", "OpenSpecFun_jll"]
git-tree-sha1 = "e2cfc4012a19088254b3950b85c3c1d8882d864d"
uuid = "276daf66-3868-5448-9aa4-cd146d93841b"
version = "2.3.1"

    [deps.SpecialFunctions.extensions]
    SpecialFunctionsChainRulesCoreExt = "ChainRulesCore"

    [deps.SpecialFunctions.weakdeps]
    ChainRulesCore = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"

[[deps.StaticArraysCore]]
git-tree-sha1 = "36b3d696ce6366023a0ea192b4cd442268995a0d"
uuid = "1e83bf80-4336-4d27-bf5d-d5a4f845583c"
version = "1.4.2"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"
version = "1.10.0"

[[deps.StringManipulation]]
deps = ["PrecompileTools"]
git-tree-sha1 = "a04cabe79c5f01f4d723cc6704070ada0b9d46d5"
uuid = "892a3eda-7b42-436c-8928-eab12a02cf0e"
version = "0.3.4"

[[deps.SuiteSparse_jll]]
deps = ["Artifacts", "Libdl", "libblastrampoline_jll"]
uuid = "bea87d4a-7f5b-5778-9afe-8cc45184846c"
version = "7.2.1+1"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"
version = "1.0.3"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "OrderedCollections", "TableTraits"]
git-tree-sha1 = "cb76cf677714c095e535e3501ac7954732aeea2d"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.11.1"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"
version = "1.10.0"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.TranscodingStreams]]
git-tree-sha1 = "54194d92959d8ebaa8e26227dbe3cdefcdcd594f"
uuid = "3bb67fe8-82b1-5028-8e26-92a6c54297fa"
version = "0.10.3"
weakdeps = ["Random", "Test"]

    [deps.TranscodingStreams.extensions]
    TestExt = ["Test", "Random"]

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.WeakRefStrings]]
deps = ["DataAPI", "InlineStrings", "Parsers"]
git-tree-sha1 = "b1be2855ed9ed8eac54e5caff2afcdb442d52c23"
uuid = "ea10d353-3f73-51f8-a26c-33c1cb351aa5"
version = "1.4.2"

[[deps.WorkerUtilities]]
git-tree-sha1 = "cd1659ba0d57b71a464a29e64dbc67cfe83d54e7"
uuid = "76eceee3-57b5-4d4a-8e66-0e911cebbf60"
version = "1.6.1"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"
version = "1.2.13+1"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.8.0+1"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"
version = "1.52.0+1"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
version = "17.4.0+2"
"""

# ╔═╡ Cell order:
# ╟─05b38db8-c20d-11ee-26b0-411e18e34b64
# ╟─0c03e004-49b7-4021-8e3f-335d4c66af5c
# ╟─220ec4af-eba6-4bd4-99f0-24d711f98fbf
# ╠═fab9e974-6350-4a29-866b-e41eb2d32866
# ╠═5c14f53f-c8bc-4d41-80d9-5a4861bbec75
# ╠═0bfe1a88-0ee4-41b6-a343-480cefcd5f69
# ╠═e27da409-8d36-46f2-9bdd-64653f113009
# ╟─647e16a3-7c4a-4440-8e37-fb59b4cc8832
# ╟─5cfad693-3690-4624-aafe-b24dfc45958a
# ╠═a2b9fb91-19f7-43aa-82de-891747fd3271
# ╠═34784318-558e-4b5c-a3eb-63edfea858f9
# ╠═4560730f-8e4e-4268-ae56-9e8f7220d580
# ╟─d384bede-389c-4e15-bde7-3f17a73a7e88
# ╟─964788c3-bd1f-4ce0-a462-52fce10099e7
# ╠═4cc7ea24-9ea4-4f6e-a5ec-4be45f08186a
# ╠═5f0bff46-e908-460b-a95f-e038e705b30f
# ╠═811d17a7-fd3a-457a-b7e8-4f6e6d22d101
# ╟─44821523-66ed-4f1a-8374-9798097b041f
# ╟─e17f7f32-9e8c-4d3c-9810-36a3f54b5464
# ╟─5b45dd03-7218-456a-ab6c-5fd65ec1bb51
# ╟─aaab6f18-8297-490b-a122-ca6175c77aa6
# ╟─d32f1afc-9c93-423e-b550-da6bd80e0e28
# ╠═a0e4962c-c549-4a15-9fec-927c33df8dc1
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
