\documentclass[11pt]{article}

\usepackage{color}
\usepackage{url}
\usepackage[margin=0.7in]{geometry}
\usepackage{enumitem}
\usepackage[colorlinks=true, bookmarks=true,  plainpages=false, pdfpagelabels=true, pageanchor=false,hypertexnames=false,linkcolor=blue,citecolor=blue,pdfauthor={Marek Petrik},urlcolor=blue]{hyperref}
\usepackage{booktabs}
%\usepackage{gfsdidot}
\usepackage[T1]{fontenc}
%\usepackage{newpxtext,mathpazo}

\setlength{\parskip}{3mm plus1mm minus1mm}
\setlength{\parindent}{0cm}

\date{}

\begin{document}
\begin{center}
	{\Large\bf CS/MATH 757/857: Mathematical Optimization for Applications}\\
	Spring 2024\\
	\textbf{Website:} \url{https://gitlab.com/marekpetrik/opt-spring2024}
\end{center}

\section{Basic Course Information}

\begin{itemize}
\item \textbf{Department}: Computer science and Mathematics
\item \textbf{Course title}: CS/Math 757 and CS/Math 857
\item \textbf{Number of credits}: 4 for CS/Math 757 and 3 for CS/Math 857
\item \textbf{Semester}: Spring 2024
\item \textbf{Modality}: In person. There is no plan for Zoom sessions or lecture recordings, unless in exceptional circumstances.
\end{itemize}

\section{Description of the Course}

\subsection{Introduction}
This class will teach you about how to formulate problems as optimization and then solve them. It will teach or reinforce declarative thinking which separates the objective being sought from the algorithms used to compute it. The class will focus on both first- and second-order methods for unconstrained and constrained problems. We will also cover some convex analysis. 

\subsection{Student learning outcomes}

The students will learn the fundamentals of mathematical optimization algorithms and analysis. They will also gain additional mathematical maturity with reading and creating more advanced mathematical material and also familiarity with applications of calculus and linear algebra. 

\subsection{Format of the course}

Lectures, assignments, and online discussion.

Class attendance is important for your learning. You are responsible for all course
assignments and meeting all deadlines unless exceptions are agreed upon with the
instructor ahead of time. Attendance in this course is optional.

\section{Prerequisites}

The most important prerequisites are good comfort with calculus, mathematical proofs, and some familiarity with linear algebra. Students also need to be able to code in a scripting language, such as Python, MATLAB, or Julia.  

\section{Basic Instructor Information}

\begin{itemize}
\item \textbf{Name of the instruction}: \emph{Marek Petrik}
\item \textbf{Office address and phone number}: Kingsbury N215b and (603) 862-2682
\item \textbf{Email address}: \url{mailto:mpetrik@cs.unh.edu}. Please do not send emails about complicated issues if it may be easier to resolve them during office hours.
\item \textbf{Office hours}: \emph{Tentative}: check the website for up to date info
  \begin{enumerate}
   \item Assignment help: Mon 11am--12pm in N215b
   \item Individual consulting: Wed 11am--12pm in N215b
  \end{enumerate} 
\item \textbf{Preferred method of contact}: In person during office hours
\item \textbf{Names and contact of TA}: \emph{Ahmed Ismail}, 0.5 TA.
\end{itemize}

The \emph{fastest} way to get your question about course material answered is to post it on the discussion forum listed on the website. Please do not email the instructor or the TAs with questions that may also be of interest to others. If you have personal questions, it is best to come to the office hours.

\section{Course Requirements}

\subsection{Nature of assignments and exams}

There will be homework assignments due approximately every week. The assignments can be turned in either in the class or online. The assignments will primarily focus on theoretical questions.

The exams (mid-term and final) will be in person. The midterms will take place halfway through the semester and the specific date will be announced at least two weeks in advance. The final exam will be scheduled according to UNH policies.

The class also involves a class project which includes several deliverables and culminates in a presentation either in the form of a poster or an oral presentation. Most of the project work will happen in October. 

\subsection{Federal credit hour requirement}

The class is expected to take about 12 hours of work a week including lectures, reading the study materials, and completing assignments and other coursework.

\subsection{Deadlines and test dates}

There will be an in-person midterm exam and an in-person final exam. These will be \textbf{closed-book} exams. Please see the class website and MyCourses for specific dates for the exams, assignments, and quizzes. No plagiarism is allowed when completing exams.

\subsection{Description of grading procedures}

\subsubsection{Late policy}
\begin{enumerate}[nosep]
\item \textbf{Late penalty:} 5\% per \textbf{hour}.
\item Start early. Do not put it off until the last moment.
\end{enumerate}

If you have a serious reason for not being able to complete an assignment or a quiz, or have other concerns about succeeding in the class, please do not hesitate to contact the instructor.

\subsubsection{Assignments}
\begin{enumerate}[nosep]
    \item One worst grade in assignments 1-7 and another one in assignments 8-14 will be dropped.
    \item If you have a legitimate reason for why you cannot finish an assignment on time, please talk to the instructor.
    \item Assignments are due on the day and hour indicated on myCourses.
    \item Preferably, complete the assignments on paper by hand. You can scan them and submit them online. If you would like to typeset them, I recommend LaTeX. It is not needed, however. 
    \item Assignments should be turned in as a \textbf{PDF} and a \textbf{separate source code file} for any coding assignments.
    \item Please do not copy solutions from online sources or your friends; it defeats the purpose of the assignments and you will not learn the material.
    \item Collaborate with your classmates, but do not blindly copy from them. Copy verbatim (or with small changes) from online sources is also not wrong. Each homework solution should be prepared independently.
\end{enumerate}

\paragraph{Programming}
The class will involve hands-on data analysis using machine learning methods. The recommended language for programming assignments is \emph{Julia}. No prior experience should be necessary and most of the code that we will work on will be quite basic. You may be able to complete them in Python or MATLAB, but there are no guarantees.

\subsubsection{Project}

The class will involve a project with multiple groups of up to 3 people working as a team. The class project will have a short report and presentation due at the end of the semester.

\subsubsection{Final grade}
The final grades will be computed using the standard UNH scale.

\subsection{Components of final grade}

The weights of individual components in computing the grade are as follows:
\begin{itemize}
\item \textbf{30\%} Homework assignments  
\item \textbf{10\%} Class project 
\item \textbf{30\%} Midterm exam  
\item \textbf{30\%} Final exam 
\end{itemize}

The exams will be in-person and on paper involving only theoretical questions. The exam questions will be very similar to homework assignments. 

\subsection{Academic honesty} Please, be honest. No forms of cheating or plagiarism will be tolerated. You are encouraged to talk to your classmates about the problems as well as search the internet. \textbf{However, please do not seek out solution guides, and if you come across one do not study it.} If your solution is too similar to the ones that can be found online, we will have to assume that your solution was copied.

\begin{itemize}[noitemsep]
	\item University Academic Honesty Policy:\\ \url{https://www.unh.edu/student-life/handbook/academic/academic-honesty}
	\item Tutorial on Plagiarism: \\  \url{http://cola.unh.edu/plagiarism-tutorial-0}
\end{itemize}


\section{Textbooks}


\begin{enumerate}
\item \emph{Dmitri P. Bertsimas (2016). Nonlinear Programming}, \textbf{3rd edition}. You should be able to buy a copy in the UNH/Durham bookstore and the library. I am not aware of a public PDF resource.
\item \emph{Boyd, S., \& Vandenberghe, L. (2004). Convex Optimization}. PDF available at \url{https://web.stanford.edu/~boyd/cvxbook/} 
\end{enumerate}






\end{document}
