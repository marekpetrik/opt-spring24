# MATH/CS 757/857: Mathematical Optimization for Applications #

### When and Where

Lectures: MWF 10:10am - 11:00am

Where: Kingsbury N113 

### Office Hours

- Office hour 1: Fri 2:30pm-3:30pm in N233
- Office hour 2: Mon 11am-12pm in N242

See [syllabus](syllabus/syllabus.pdf) for the information on grading, rules, and office hours.

## Syllabus: Lectures

Notes *may* be uploaded after each lecture. *Please take your own notes*.

Regarding *reading materials* see the section on textbooks below.


| Date   | Day | Topic                                                                               | Reading     |
|--------|-----|-------------------------------------------------------------------------------------|-------------|
| Jan 24 | Wed | [Math background: Calculus](slides/class1/class1_ann.pdf)                           | NP A, A.5   |
| Jan 26 | Fri | [Math background: Linear algebra](slides/class2/class2_ann.pdf)                     | NP A.1-A.5  |
| Jan 29 | Mon | [Optimality conditions: Necessary](slides/class3/class3_ann.pdf)                    | NP 1.1      |
| Jan 31 | Wed | [Optimality conditions: Convexity](slides/class4/class4_ann.pdf)                    | NP 1.1      |
| Feb 02 | Fri | [Optimality conditions: Sufficient](slides/class5/class5_ann.pdf)                   | NP 1.2      |
| Feb 05 | Mon | [Gradient methods: Descent direction](slides/class6/class6_ann.pdf)                 | NP 1.2      |
| Feb 07 | Wed | [Gradient methods: Line search](slides/class7/class7_ann.pdf)                       | NP 1.2      |
| Feb 09 | Fri | [Gradient methods: Convergence](slides/class8/class8_ann.pdf)                       | NP 1.3      |
| Feb 12 | Mon | [GD: Convergence rate of strongly convex](slides/class9/class9_ann.pdf)             | NP 1.4      |
| Feb 14 | Wed | [Beyond GD: Convergence rate, proximal view](slides/class10/class10_ann.pdf)        | NP 1.4      |
| Feb 16 | Fri | [Newton, quasi-newton and conjugate gradient](slides/class11/class11_ann.pdf)       | NP 2.2      |
| Feb 19 | Mon | [Convex functions and sets](slides/class12/class12_ann.pdf)                         | CO 2.1, 3.1 |
| Feb 21 | Wed | [Convex sets](slides/class13/class13_ann.pdf)                                       | CO 2.2      |
| Feb 23 | Fri | [Convex sets and projections](slides/class14/class14_ann.pdf)                       | CO 2        |
| Feb 26 | Mon | [Convex functions: basic definitions](slides/class15/class15_ann.pdf)               | CO 3        |
| Feb 28 | Wed | [Preserving convexity](slides/class16/class16_ann.pdf)                              | CO 3        |
| Mar 01 | Fri | [Convex conjugates and quasi-convexity](slides/class17/class17_ann.pdf)             | CO 3        |
| Mar 04 | Mon | [Convex constrained optimality](slides/class18/class18_ann.pdf)                     | NP 3.1      |
| Mar 06 | Wed | [Convex constrained optimality: general cases](slides/class19/class19_ann.pdf)      | NP 3.1-2    |
| Mar 08 | Fri | Convex constrained algorithms: Frank-Wolfe, Projected Gradient, Proximal algorithms | NP 3.3,6    |
| Mar 11 | Mon | [Midterm review](slides/midterm/midterm1.pdf)                                       |             |
| Mar 13 | Wed | [Midterm review](slides/midterm/midterm1.pdf)                                       |             |
| Mar 14 | Thu | *Midterm* (common exam period)                                                      |             |
| Mar 15 | Fri | No class (or an optional discussion)                                                |             |
| Mar 18 | Mon | *break*                                                                             |             |
| Mar 20 | Wed | *break*                                                                             |             |
| Mar 22 | Fri | *break*                                                                             |             |
| Mar 25 | Mon | Project proposals                                                                   |             |
| Mar 27 | Wed | Midterm discussion                                                                  |             |
| Mar 29 | Fri | [Lagrange multipliers](slides/class20/class20_ann.pdf)                              | NP 4.1      |
| Apr 01 | Mon | [Lagrange multiplier proof](slides/class21/class21_ann.pdf)                         | NP 4.1      |
| Apr 03 | Wed | [Sufficiency and KKT conditions](slides/class22/class22_ann.pdf)                    | NP 4.2-3    |
| Apr 05 | Fri | [Sufficiency and KKT conditions](slides/class22/class22_ann.pdf)                    | NP 4.3      |
| Apr 08 | Mon | *No class*                                                                          |             |
| Apr 10 | Wed | [Duality: Linear constraints](slides/class23/class23.pdf)                           | NP 4.4      |
| Apr 12 | Fri | [Duality and dual problems](slides/class24/class24.pdf)                             | NP 4.4      |
| Apr 15 | Mon | [Weak and strong duality](slides/class25/class25.pdf)                               | NP 4.4      |
| Apr 17 | Wed | [Duality examples](slides/class26/class26.pdf)                                      | NP 4        |
| Apr 19 | Fri | [Linear programing](slides/class27/class27.pdf)                                     | NP 6        |
| Apr 22 | Mon | [General duality conditions](slides/class28/class28.pdf)                            | NP 6        |
| Apr 24 | Wed | [Linear programs and convex conjugates](slides/class29/class29.pdf)                 | NP 5,6      |
| Apr 26 | Fri | [Applications](slides/class30/class30.pdf)                                          |             |
| Apr 29 | Mon | Project presentations                                                               |             |
| May 01 | Wed | Project presentations                                                               |             |
| May 03 | Fri | Final exam review                                                                   |             |
| May 06 | Mon | Final exam review                                                                   |             |


## Office Hours

- *Piazza*: [https://piazza.com/unh/spring2024/csmath757857](https://piazza.com/unh/spring2024/csmath757857)

## Assignments

| Assignment                         | Source                             | Due Date            |
|------------------------------------|------------------------------------|---------------------|
| [1](assignments/assignment1.pdf)   | [1](assignments/assignment1.tex)   | Wed 1/31 at 11:59PM |
| [2](assignments/assignment2.pdf)   | [2](assignments/assignment2.tex)   | Wed 2/07 at 11:59PM |
| [3](assignments/assignment3.pdf)   | [3](assignments/assignment3.tex)   | Wed 2/14 at 11:59PM |
| [4](assignments/assignment4.pdf)   | [4](assignments/assignment4.tex)   | Wed 2/21 at 11:59PM |
| [5](assignments/assignment5.pdf)   | [5](assignments/assignment5.tex)   | Wed 2/28 at 11:59PM |
| [6](assignments/assignment6.pdf)   | [6](assignments/assignment6.tex)   | Wed 3/06 at 11:59PM |
| [7](assignments/assignment7.pdf)   | None                               | Wed 3/13 at 11:59PM |
| [8](assignments/assignment8.pdf)   | [8](assignments/assignment8.tex)   | Wed 3/27 at 11:59PM |
| [9](assignments/assignment9.pdf)   | [9](assignments/assignment9.tex)   | Wed 4/03 at 11:59PM |
| [10](assignments/assignment10.pdf) | [10](assignments/assignment10.tex) | Wed 4/10 at 11:59PM |
| [11](assignments/assignment11.pdf) | [11](assignments/assignment11.tex) | Wed 4/17 at 11:59PM |
| [12](assignments/assignment12.pdf) | [12](assignments/assignment12.tex) | Wed 4/24 at 11:59PM |
| Skip                               | Skip                               | Wed 5/01 at 11:59PM |
| [14](assignments/assignment14.pdf) | [14](assignments/assignment14.tex) | Mon 5/06 at 11:59PM |

## Project

The class will involve a project with multiple groups of up to 3 people working as a team. The class project will have a short report and presentation due at the end of the semester.

## Exams

The exams (mid-term and final) will be in person. The midterms will take place halfway through the semester and the specific date will be announced at least two weeks in advance. The final exam will be scheduled according to UNH policies.

## Textbooks ##

### Main References:
- **NP**: Bertsekas, D. (2016) Nonlinear programming, 3rd edition.
- **CO** Boyd, S., & Vandenberghe, L. (2004). [Convex Optimization](http://web.stanford.edu/~boyd/cvxbook/)

### Linear Algebra:
- **LA**: Strang, G. [Introduction to Linear Algebra](http://math.mit.edu/~gs/linearalgebra/). (2016) *Also see:* [online lectures](https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/video-lectures/)
- **LAO**: Hefferon, J. [Linear Algebra](http://joshua.smcvt.edu/linearalgebra/#current_version) (2017)
- Horn, R. A., & Johnson, C. A. (2013). Matrix analysis (2nd ed.). Cambridge University Press.

### Convex Analysis:
- Rockafellar, R. T. (1970). Convex analysis.
- Rockafellar, R. T., & Wets, R. J. (2009). Variational analysis.

### Convex Optimization (including proximal methods):
- **PA**: Parikh, N., & Boyd, S. (2013). [Proximal algorithms](https://web.stanford.edu/~boyd/papers/pdf/prox_algs.pdf). Foundations and Trends in Optimization, 1(3), 123–231. 
- **LCO** Nesterov, Y. (2018). Lectures on Convex Optimization (2nd ed.).



### Other Mathematical Optimization:
- **NO** Nocedal, J., Wright, S. (2006). [Numerical Optimization](https://www.springer.com/us/book/9780387303031)

## Programming Language

We will use [Julia](https://julialang.org/). Good IDEs are VS Code, EMacs, and Vim. You may be able to complete most of the assigments in Python or MATLAB, but I am unaware of a good alternative to (JuMP)[https://jump.dev/] for those languages.

