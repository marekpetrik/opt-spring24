\documentclass[9pt]{beamer}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathtools}

\usepackage{array}
%\usepackage{enumitem}

\newcommand{\Real}{\mathbb{R}}


\title{Applications of Mathematical Optimization}
\author{Marek Petrik}

\newcommand{\opt}{^\star}
\newcommand{\tr}{^\top}
\newcommand{\st}{\quad\operatorname{s.t.}\quad}
\newcommand{\eye}{I}
\newenvironment{mprog}{\begin{array}{>{\displaystyle}l>{\displaystyle}l>{\displaystyle}l}}{\end{array}}
\newcommand{\subjectto}{\operatorname{s.\,t.} &}
\newcommand{\minimize}[1]{\min_{#1} &}
\newcommand{\maximize}[1]{\max_{#1} &}
\newcommand{\cs}{\\[1ex] & }
\newcommand{\stc}{\\[1ex] \subjectto}
\newcommand{\real}{\mathbb{R}}
\newcommand{\E}[1]{\mathbb{E}[#1]}



% Define risk operator
\newcommand{\risko}{\operatorname{Risk}}
\DeclareMathOperator{\varo}{VaR}
\DeclareMathOperator{\cvaro}{CVaR}
\DeclareMathOperator{\evaro}{EVaR}
\DeclareMathOperator{\ermo}{ERM}

\renewcommand{\P}[1]{\mathbb{P}\left[ #1 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}
\newcommand{\risk}[1]{\risko\left[#1\right]}
\newcommand{\erm}[2]{\ermo_{#1}\left[#2\right]}
\newcommand{\var}[2]{\varo_{#1} \left[#2\right]}
\newcommand{\cvar}[2]{\cvaro_{#1} \left[#2\right]}
\newcommand{\evar}[2]{\evaro_{#1} \left[#2\right]}


\setbeamertemplate{navigation symbols}{}
\usefonttheme{professionalfonts}
\setbeamertemplate{footline}[frame number]

\begin{document}

\begin{frame}
\maketitle
\end{frame}

\begin{frame} \frametitle{Basic Machine Learning}

  \begin{enumerate}
  \item Linear regression: \pause 1st-order optimality conditions
    \vfill 
  \item Logistic regression, GLM: \pause Convex unconstrained minimization, L-BFGS
    \vfill 
  \item Lasso: \pause Large convex (non-differentiable optimization), sub-gradient methods, quadratic programs, min-max algorithms
    \vfill 
  \item SVM: \pause quadratic programs, very large
    \vfill 
  \item Neural nets: \pause non-convex, accelerated stochastic gradient descent
  \end{enumerate}
\end{frame}


\begin{frame} \frametitle{Quantile Regression}
  \begin{center}
  \includegraphics[width=\linewidth]{../fig/apps/quantileregression.png}
  {\tiny source:
    \url{https://www.linkedin.com/pulse/quantile-regression-workflows-r-video-anton-antonov/}}
    \end{center}
  \vfill 
  \begin{center}
  \includegraphics[width=\linewidth]{../fig/apps/quantilelp.png}
  {\tiny source: \url{https://en.wikipedia.org/wiki/Quantile_regression}}
  \end{center}
\end{frame}


\begin{frame} \frametitle{Inventory Models}
  \begin{itemize}
  \item Newsvendor problem: perishable items
  \[
   \max_{q\in \Real} \operatorname{profit} := \E{ p \cdot \min \left\{ q, D \right\} }  - c \cdot  q
  \]
\item Inventory management: non-perishable items
  \[
   \max_{q\in \Real} \operatorname{profit} := \E{ p \cdot \min \left\{ q, D \right\} + v([q - D]_+) }  - c \cdot  q
  \]
  \item Revenue management: Customer choice model of $D(p)$ of $p$
  \[
   \max_{p\in \Real} \operatorname{profit} := \E{ p \cdot D(p)  } 
  \]
  \item Combined inventory and price management 
  \item Network inventory model, network of warehouses: Linear program
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Optimal Facility Location}
  \begin{center}
  \includegraphics[width=0.5\linewidth]{../fig/apps/facility.png}
  {\tiny source:
\url{https://towardsdatascience.com/optimization-capacitated-facility-location-problem-in-python-57c08f259fe0} }
    \end{center}
  \vfill 
  \begin{center}
  \includegraphics[width=0.6\linewidth]{../fig/apps/facilitymilp.png}
  {\tiny source: \url{https://en.wikipedia.org/wiki/Optimal_facility_location}}
  \end{center}
\end{frame}

\begin{frame} \frametitle{Portfolio Models}
  \begin{itemize}
  \item Risk-neutral resource allocation
    \[
     \max_{x\in \Delta^n} \, \E{\tilde{p}\tr x }
    \]
  \item Markowitz (``Modern'') Portfolio Theory
    \[
     \max_{x\in \Delta^n} \, \E{\tilde{p}\tr x} \quad \operatorname{s\,.t.} \quad \mathbb{V}[\tilde{p}\tr x] \le \sigma^2
    \]
  \item Risk measures
    \[
     \max_{x\in \Delta^n} \, \E{\tilde{p}\tr x} \quad \operatorname{s\,.t.} \quad \operatorname{Risk}[\tilde{p}\tr x] \ge t
    \]
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Measuring Risk}
  \begin{itemize}
  \item Value at Risk
   \begin{equation*}
    \var{\alpha}{\tilde{x}}
    = \sup \left\{ t \in \Real \;:\; \Pr{\tilde{x} \ge t} \ge 1-\alpha \right\}
   \end{equation*}
  \item Conditional Value at Risk
  \begin{align*}
    \cvar{\alpha}{\tilde{x}}
    &\; =\;  \sup_{z\in\mathbb R}\left(z - \frac{1}{\alpha} \E{ z - \tilde{x} }_{+} \right) \\
    &\; \approx\;  \E{\tilde{x} \mid  \tilde{x} \le \var{\alpha}{\tilde{x}}}
\end{align*}
  \item Entropic Value at Risk
  \[
    \evar{\alpha}{\tilde{x}} \;=\;  \sup_{\beta>0}\left(\erm{\beta}{\tilde{x}} + \beta^{-1} \log  \alpha\right).
  \]
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Network Inventory Models: Reserviors}
{\tiny Maceiral, et al., (2018). Twenty Years of Application of Stochastic Dual Dynamic Programming in Official and Agent Studies in Brazil-Main Features and Improvements on the NEWAVE Model. 2018 Power Systems Computation Conference (PSCC).}

  \begin{center}
  \includegraphics[width=0.3\linewidth]{../fig/apps/brazilhydro.png}\\
  {\tiny source: \url{https://www.mdpi.com/2073-4441/10/5/592}}
    \end{center}

Objective:
\begin{equation*} \varphi_{t}(x_{t-1},\xi_{t})=\min\limits_{x_{1}}(1-\lambda)E[Z (x)]+\lambda \operatorname{CVaR}_{\alpha}[Z (x)]
\end{equation*} 
\end{frame}

\begin{frame} \frametitle{Markov Decision Process}
  \textbf{Model}  \par
    {\small
   ~~~States $\mathcal{S}$: $s_1, s_2, s_3, \dots $ \par
   ~~~Actions $\mathcal{A}$: $a_1, a_2, \dots $ \par
   ~~~Transition probabilities $p$ \par
   ~~~Rewards $r$}
    \vfill 
    \textbf{Solution}: Policy $\pi\colon \mathcal{S} \to \mathcal{A}$  (randomized in general) 
    \vfill
    \textbf{Return}: Discounted random return (random over trajectories):
    \[
      \tilde{\rho}(\pi) = \sum_{t=0}^\infty \gamma^t r(\tilde{s}^{\pi}_t, \tilde{a}^{\pi}_t)
    \]
\end{frame}

\begin{frame}{Business Applications of RL}
  \begin{itemize}
  \item Determine how to collect tax debt \\
    {\tiny Abe, N., Kowalczyk, M., Domick, M., Gardinier, T., Melville, P., Pendus, C., Reddy, C. K., Jensen, D. L., Thomas, V. P., Bennett, J. J., Anderson, G. F., & Cooley, B. R. (2010). Optimizing debt collections using constrained reinforcement learning. ACM SIGKDD International Conference on Knowledge Discovery and Data Mining.}
    \vfill
  \item Optimal mortgage refinance strategy
  \end{itemize}
\vfill
\textbf{Solutions}: Linear program optimization
\end{frame}

\begin{frame} \frametitle{Optimal Energy Generation}
  \begin{itemize}
  \item ISO New England: \url{https://www.iso-ne.com/}
  \item Optimal power flow: \url{https://ieeexplore-ieee-org.unh.idm.oclc.org/document/9649715}
\item Unit commitment problem: \url{https://www.nrel.gov/docs/fy23osti/85006.pdf}
  \end{itemize}
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: LaTeX
%%% TeX-master: t
%%% End:
